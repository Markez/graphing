# Django graphing app with live polling

Creating a basic Django app that exposes a RESTful API endpoint for a random number generator and display a graph using a streaming plot.ly graph.

The project will use Django Rest Framework, Django Channels, Channels API, and Plot.ly

# Resources
* https://github.com/linuxlewis/channels-api
* https://www.django-rest-framework.org/
* https://github.com/plotly/Streaming-Demos
* https://codepen.io/etpinard/pen/qZzyXp

# Pre-requisites
* Python
* Django=2.5


To view the graph setup the django environment and Install required packages from:
* requirements.txt
* Then visit the following url: http://127.0.0.1:8000/graph/v1/play/