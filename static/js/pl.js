// function rand() {
//   return Math.random();
// }
function rand() {
    // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest();

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', 'http://127.0.0.1:8000/v1/coordinate/points/', true);

    request.onload = function () {
      // Begin accessing JSON data here
      var data = JSON.parse(this.response);

      if (request.status >= 200 && request.status < 400) {
        return data.y;
      } else {
        console.log('error');
      }
    };

    // Send request
    request.send();
}

Plotly.plot('graph', [{
  y: [1,2,3].map(rand)
}, {
  y: [1,2,3].map(rand)
}]);

var cnt = 0;

var interval = setInterval(function() {

  Plotly.extendTraces('graph', {
    y: [[rand()], [rand()]]
  }, [0, 1]);

  if(cnt === 100) clearInterval(interval);
}, 300);