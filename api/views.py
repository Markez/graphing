from rest_framework.decorators import api_view
from django.http import JsonResponse

from . import serializers
import logging
import random

logging = logging.getLogger('processes')


@api_view(['GET', 'POST'])
def Coordinates(request):
    """
    Code to generate and return the random points
    """
    if request.method == 'GET':
        response = {}
        logging.info("=====================================")
        # points = [{"y": random.randint(0, 10), "y1": random.randint(0, 10)}]
        response['y'] = random.uniform(0, 1)
        status = 200
        logging.info(response)
        # results = serializers.PointsSerializer(points, many=True).data
        return JsonResponse(response, status=status, safe=False)

    elif request.method == 'POST':
        pass