import plotly.plotly as py
import plotly.figure_factory as ff
from polling.random import Random

import numpy as np

list = []
list = Random.RandomNumber()
# x = np.linspace(list)
# y = np.linspace(list)

x = np.linspace(-3, 3, 100)
y = np.linspace(-3, 3, 100)
Y, X = np.meshgrid(x, y)
u = -1 - X**2 + Y
v = 1 + X - Y**2

# Create streamline figure
fig = ff.create_streamline(x, y, u, v, arrow_scale=.1)
py.plot(fig, filename='Streamline Plot Example')