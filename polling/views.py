from django.shortcuts import render
import logging

logging = logging.getLogger('processes')


def graghpage(request):
    try:
        if request.method == 'GET':
            return render(request, 'graph.html')
        elif request.method == 'POST':
            logging.info('Ready to show graph')
            pass
        else:
            logging.warning("Un-supported request")
    except Exception as e:
        logging.exception(e)
        pass
